# Raízen - ETL 



## Introdução

O objetivo é estabelecer um pipeline de processamento de um arquivo xlsx definido segundo a documentação em 

[https://github.com/raizen-analytics/data-engineering-test/blob/master/TEST.md](Documentação do teste)


Esse código está preparado para executar em plataforma **Linux**. 


O código-fonte existe de duas formas: 


* Jupyter notebook
* shell/python scripts



Embora funcional, o notebook foi utilizado para o desenvolvimento inicial como forma de visualização e demonstração da lógica utilizada e para facilitar a produção da primeira versão do código.  O código se localiza em `nasa-logs.ipynb` e está no formato de notebook do `jupyter`



Os scripts shell/python representa o que estaria em "produção".  Existe um Shellscript em `bash` que é um orquestrador que configura ambiente e executa o script *pySpark* através do comando `spark-submit`. 



## Pré-requisitos

* git
* Hadoop 2.8x
* Spark 2.4x
* Python3x
  * jupyter
  * pip
  * pytest
    * pytest-spark



## Instalação



### Clonando o repositório



`git clone git@bitbucket.org:carneirao666/raizen.git`



### Instalando a biblioteca pyspark

Na linha de comando, execute:

* `pip install pyspark`

Se tiver problemas com permissão, execute:

* `pip install pyspark --user`



### Variáveis de ambiente do pySpark

É preciso exportar algumas variáveis de ambiente para que o pySpark funcione corretamente. Se preferir, adicione as seguintes linhas abaixo no seu arquivo `$HOME/.bashrc`



```bash
export PYSPARK_PYTHON=python3
export PYSPARK_DRIVER_PYTHON=ipython3
export PYTHONPATH=$SPARK_HOME/python:$SPARK_HOME/python/lib/py4j-0.10.4-src.zip
export PYSPARK_DRIVER_PYTHON_OPTS=notebook

```

**ATENÇÃO: **As versões podem variar conforme a configuração do **seu** sistema operacional. 



## Organização de diretórios e arquivos



**Diretórios e arquivos do projeto**

```bash
.
├── assets
│   └── img
├── etl
│   ├── conf
│   │   └── default.json
│   ├── create_tables.sh
│   ├── ddl
│   │   ├── create_raw_table.hql
│   │   ├── create_sales_diesel_by_uf_type.hql
│   │   ├── create_sales_odf_uf_product.hql
│   │   └── create_stage_table.hql
│   ├── etl.py
│   ├── __init__.py
│   ├── jars
│   ├── local_ref
│   │   └── datasets
│   ├── mock_remote
│   │   └── raizen2.xlsx
│   ├── requirements.txt
│   ├── start.sh
│   └── utils
│       ├── base.py
│       ├── file_tools.py
│       ├── __init__.py
│       └── __pycache__
├── notebooks
│   ├── etl-prototype.ipynb
│   └── raizen2.xlsx
└── README.md
```

Principais diretórios

* **local_refs/datasets**: Trata-se do diretório-padrão para o downloads do dataset
* **mock_remote**: mock de um ponto de montagem remoto
* **etl**: É o diretório dos arquivos-fonte, ou seja, do código de "produção". Tanto de código `Python` quanto `Shellscript`
* **notebooks**: Trata-se da localização do notebook contendo a versão preliminar(dev) do código. 




**HDFS**



Por padrão, deve-se primeiro assumir um diretório-base dentro do HDFS para definir os outros diretórios do sistema. Para isso, basta exportar a variável `HDFS_BASE_DIR` . A partir desse diretório temos o seguinte:



```bash
<HDFS_BASE_DIR>/processing
<HDFS_BASE_DIR>/processed
<HDFS_BASE_DIR>/rejected
```



Esses três diretórios seguem um padrão recorrente em projetos de ETL, que é o que se trata esse desafio. A estratégia básica é a de *tudo-ou-nada*, ou seja, ou todos os arquivos devem ser processados ou o processamento como um todo falhará. Essa estratégia faz sentido, já que o resultado final depende dos dados de **todos** os arquivos.



* **processing**: Quando o processamento é iniciado, os dados são movidos do diretório de *download* para o HDFS nesse diretório, para que fique acessível ao Spark através do cluster;
* **processed**: Se o processamento dos arquivos for bem-sucedido, eles serão movidos para este diretório;
* **rejected**: Se o processamento de qualquer arquivo falhar, 



**Módulos**



Existem dois módulos com propósitos bem específicos:



* `utils.base`: Trata-se de um módulo básico representado pelo arquivo `base.py`. Ele é responsável por fornecer: sessão Spark, estrutura de configuração e estrutura de log;
* `utils.file_tools`: Trata-se de um móulo que tem ferramentas de auxílio em operações com arquivos tanto localmente quanto  no HDFS.







## Configurando 

A configuração é possível através do arquivo de configuração `conf/default.json`. Tem tipicamente o seguinte conteúdo:



```json
{

    "dataSetPaths" : [
         "mock_remote/raizen2.xlsx"
    ]
    ,"localDirs" : {
        "download" : "$LOCAL_BASE_DIR/datasets"
    }
    ,"hdfsDirs" : {
        "processing":"$HDFS_BASE_DIR/processing"
        ,"processed":"$HDFS_BASE_DIR/processed"
        ,"rejected":"$HDFS_BASE_DIR/rejected"
    }
}
```



### Seções do arquivo de configuração

* **dataSetPaths**: É o local onde está a lista de arquivos para o "download" (formato *xlsx*);
* **localDirs**: Seção reservada para a definição de diretórios locais;
* **hdfsDirs:** Seção reservada para a definição dos diretórios localizados no HDFS



Repare que existem alguns *placeholders* indicados pelo prefixo '$' . Isso foi uma necessidade extra que achei interessante implementar. Essas variáveis são recupearadas do ambiente, internamente através do módulo `base`(Veja na seção 'Organização de diretórios e arquivos') e . Por isso é importante que as variáveis de ambiente estejam exportadas antes da execução do script `start.sh`





## Setup e Execução



1. As variáveis `LOCAL_BASE_DIR`(ex: export `LOCAL_BASE_DIR=local_ref`) e `HDFS_BASE_DIR`(veja o arquivo `start.sh` precisam estar exportadas. Certifique-se disso!
2. Verifique o arquivo de configuração `conf/default.json`
3. Verifique as configurações do `spark-submit` no arquivo `start.sh`
4. Execute o script `create_tables.sh` para criar as tabelas no hive
5. De dentro do diretório `etl/`, execute o comando `/bin/bash start.sh`
6. Um resumo dos dados deve ser exibido referentes ao desafio proposto



## Provisionamento(Docker)


Depois de clonar o repositório, e execute:

```bash

$ docker pull ramtricks/hadoop-bootstrap:0.0.16

... Isso vai demorar

$ docker network create x

$ docker run --network x -v `pwd`/raizen:/home/hduser/raizen --name hadoop -p 8080:8080 -uroot -w /root -d -it ramtricks/hadoop-bootstrap:0.0.16 /bin/bash -c "yes | ./start-hadoop.sh"

```

Verifique se o container está rodando com o comando `docker ps -a`

```bash
CONTAINER ID        IMAGE                               COMMAND             CREATED             STATUS              PORTS                                                                                                                   NAMES
bf735884e9d7        ramtricks/hadoop-bootstrap:0.0.16   "/bin/bash"         2 minutes ago       Up 2 minutes        5000/tcp, 8088/tcp, 8888/tcp, 9021/tcp, 27019/tcp, 50030/tcp, 50060/tcp, 50070/tcp, 50075/tcp, 0.0.0.0:8080->8080/tcp   hadoop

```


Aguarde mais ou menos um minuto para dar tempo de todos os serviços do 'core' do Hadoop subir, e então execute:


### Spark no terminal

`docker exec -uhduser -w/home/hduser/raizen/etl -it 060d753802fa bash`

Agora execute

`./start.sh`


### Jupyter

`docker exec -uhduser -w/home/hduser/raizen/notebooks -it 060d753802fa bash`


Subindo o Jupyter

```bash

huser:$ jupyter-notebook --port 8080 --ip `hostname -i`

[I 12:35:35.618 NotebookApp] Serving notebooks from local directory: /home/hduser/raizen/notebooks
[I 12:35:35.618 NotebookApp] 0 active kernels
[I 12:35:35.618 NotebookApp] The Jupyter Notebook is running at:
[I 12:35:35.618 NotebookApp] http://172.24.0.2:8080/?token=03490fe2db8ef5d917b56763c39e268c0fae3d162bf0f7b5
[I 12:35:35.618 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[W 12:35:35.619 NotebookApp] No web browser found: could not locate runnable browser.
[C 12:35:35.620 NotebookApp] 
    
    Copy/paste this URL into your browser when you connect for the first time,
    to login with a token:
        http://172.24.0.2:8080/?token=03490fe2db8ef5d917b56763c39e268c0fae3d162bf0f7b5

```

Copie o endereço e abra no browser





