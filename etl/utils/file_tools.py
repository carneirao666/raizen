import os,sys,re
from os import walk
from utils.base import *


# Globals
# This came from base module
logger = getLogger(name='tools')

def runDFSCmd(cmd):
    """A facilitator to run 'hdfs dfs' commands. Runs using 'subprocess' module
        to capture system messages and errors

        Parameters
        ----------

        cmd: str, required
        A string with 'hdfs dfs' command.

        
        Returns
        -------
        A system message in the case of success.


        Raises
        ------

        An generic Exception with error message.


    """
    import subprocess as sp

    hdfsCmd = 'hdfs dfs ' + cmd
    pipe = sp.Popen( 
        hdfsCmd
        , shell=True
        , stdout=sp.PIPE
        , stderr=sp.PIPE 
    )
    res = pipe.communicate()
    code = pipe.returncode
    error = str(res[1])

    # Getting error from system
    if code > 0 and error:
        raise Exception(error)

    # Getting res rows, if exists
    msgList = []
    if len(res[0]) > 0:
        r = str(res[0])
        msgList = re.split(r"\\n",r)
        if msgList[-1] == "'":
            msgList.pop(-1)

    return msgList



def downloadDataSets(dataSetUrls=[],downloadPath=None):
    """Downloads datasets from NASA's ftp
    
        Parameters
        ----------
        
        dataSetUrls :   List(str), required
                        A list of datasets URLs strings
            
        Returns
        -------

        nothing

    """
    import ftplib
    
    # checking parameters
    if downloadPath == None or downloadPath == '':
        logger.error("downloadPath is required")
        sys.exit(0)
    
    for ds in dataSetUrls:
        # Getting all necessery FTP data using regex groups
        r = re.search(r'^ftp://(.+?)/(.+?/)(NASA_.+?)$',ds)
        
        # Putting groups data on variables
        path = r.group(2)
        filename = r.group(3)
        ftpAddr = r.group(1)
        
        
        # Checking if filepath exists
        filepath = '/'.join([downloadPath, filename])
        logger.info("Verificando '{}'".format(filepath))
        if os.path.exists(filepath):
            logger.warn ("O arquivo '{}' já existe!".format(filepath))
        else:
            logger.info("Baixando '{}' para '{}'".format(ds,filepath))
            try:
                # Downloanding a dataset
                ftp = ftplib.FTP(ftpAddr) 
                ftp.login("anonymous", "anonymous") # it's a public FTP, so user is always anonymous 
                ftp.cwd(path)
                ftp.retrbinary("RETR " + filename, open( filepath, 'wb').write)
                ftp.quit()
            except Exception as e:
                logger.error("Erro FTP: {}".format(str(e)))
                sys.exit(1)

                
                
def decompressFile(filePath):
    """Decompress a single 'gz' file using 'gunzip' command.
    
        Parameters
        ----------
            
        filepath :  str, required
        A 'zip'( file with .gz extension) file path string
        
        Returns
        -------
        
        nothing
    
    """
    # -q for 'quiet' and not-interactive mode
    logger.info("Descompactando o arquivo {}".format(filePath))
    os.system('gunzip -q {}'.format(filePath))



def decompressFiles(downloadPath):
    """Iterator for decompressFile function
    
        Parameters
        ----------
        
        downloadPath:   str, required
        Download path string
        
        Returns
        -------
            
        nothing
            
            
    """ 

    # Walking through directories
    for (dirpath, dirnames, filenames) in walk(downloadPath):
        for f in filenames:
            # Look just to .gz files
            if re.search(r'\.gz$',f):
                filepath = '/'.join([dirpath, f])
                decompressFile(filepath)
        break
        
        
        
def moveToHDFS(downloadPath,hdfsPath):
    """Stores files downloaded at 'download'(and decompressed) directory on HDFS. Ignores
        not-decompressed files(.gz). After move to HDFS, deletes the local files.
        
        Parameters
        ----------
        
        downloadPath: str, required
        Download path string(required)
            
        hdfsPath:   str, required
        HDFS path string
        
        Returns
        -------
        
        nothing
        
    """
    ## Putting files on HDFS

    # Creating HDFS paths
    print("Criando o diretório '{}' caso não exista".format(hdfsPath))
    os.system('hdfs dfs -mkdir -p {}'.format(hdfsPath))

    logger.info("Procurando arquivos em '{}'".format(downloadPath))
    for (dirPath, dirNames, fileNames) in walk(downloadPath):
        for f in fileNames:
            if not re.search(r'\.gz$',f):
                filePath = '/'.join([dirPath, f])
                logger.info("Salvando o arquivo '{}' em '{}'".format(filePath,hdfsPath))
                runDFSCmd("-put -f {} {}".format(filePath, hdfsPath))
                
                logger.info("Apagando o arquivo local '{}'".format(filePath))
                os.system("rm -f {}".format(filePath))
        break


def copyToHDFS(downloadPath, hdfsPath):
    """Stores files downloaded at 'download'(and decompressed) directory on HDFS. Ignores
        not-decompressed files(.gz).

        Parameters
        ----------

        downloadPath: str, required
        Download path string(required)

        hdfsPath:   str, required
        HDFS path string

        Returns
        -------

        nothing

    """
    ## Putting files on HDFS

    # Creating HDFS paths
    print("Criando o diretório '{}' caso não exista".format(hdfsPath))
    os.system('hdfs dfs -mkdir -p {}'.format(hdfsPath))

    logger.info("Procurando arquivos em '{}'".format(downloadPath))
    for (dirPath, dirNames, fileNames) in walk(downloadPath):
        for f in fileNames:
            if not re.search(r'\.gz$', f):
                filePath = '/'.join([dirPath, f])
                logger.info("Salvando o arquivo '{}' em '{}'".format(filePath, hdfsPath))
                runDFSCmd("-put -f {} {}".format(filePath, hdfsPath))
        break



def mkHDFSDirs(conf):

    # Reading HDFS dirs for creation
    hdfsDirs = conf['hdfsDirs']

    for k in hdfsDirs.keys():
        hdfsDir = hdfsDirs[k]
        logger.info("Criando o diretório '{}'".format(hdfsDir))
        runDFSCmd("-mkdir -p {}".format(hdfsDir))

        # TODO Checking mkdir

    return True

if __name__ == '__main__':
    pass