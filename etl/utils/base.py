import os
from pyspark.sql import SparkSession
from functools import lru_cache
import logging
import json,re


@lru_cache(maxsize=None)
def getSpark():
    """Returns a SparkSession object
    """
    spark = ( 
        SparkSession.builder
        .appName('raizen-etl')
        .config("hive.exec.dynamic.partition", "true")
        .config("hive.exec.dynamic.partition.mode", "nonstrict")
        .enableHiveSupport()
        .getOrCreate()
    )
    spark.sparkContext.setLogLevel("ERROR")
    return spark


def getLogger(
        name
        ,format= '[%(levelname)s] [%(asctime)s] - %(message)s'
        ,datefmt='%Y-%m-%d %H:%M:%S'
    ):
    """Returns a singleton object 'logging'

        Parameters
        ----------

            name: str, required
            String logger name for logging singleton

            format  str, required
            Log row format ( default: '[%(levelname)s] [%(asctime)s] - %(message)s' )

            datefmt: str, required
            Date/hour format(default: '%Y-%m-%d %H:%M:%S')

        Returns
        -------

            A 'logging' singleton object instance

    """
    logging.basicConfig(
        level   = logging.INFO,
        format  = format,
        datefmt = datefmt
    )
    logger = logging.getLogger('prediction')
    return logger


def checkEnv():
    # Checking minimal environment
    varsToCheck = [
        'LOCAL_BASE_DIR'
        ,'HDFS_BASE_DIR'
    ]

    for var in varsToCheck:
        try:
            os.environ[var]
        except Exception as e:
            print("[ERROR] Var '{}' not found! Please, read Readme.md!".format(var))
            exit(1)

    return True

def getConfig(configFile='conf/default.json'):
    """Reads a JSON config file and returns a Dictionary with a config data. Some 
        environment variables must be set to replace some 'placeholders' defined
        on config file, but with several limitations. Please, read the Readme.md for more details!

        Parameters
        ----------
        configFile: str,required
        Config file path string

        Returns
        -------
        A dictionary with JSON config file content

    """
    
    # Checking for basic environment vars
    checkEnv()

    confObj = {}
    # Checking config file
    if not os.path.exists(configFile):
        raise Exception("File '{}' not found! Please, read 'Readme.md'!'".format(configFile))
    else:
        # Trying to load config from JSON file
        try:
            with open(configFile) as json_file:
                confObj = json.load(json_file)
                for firstLevelKey in confObj.keys():
                    # This allows complex structure analysis with a big limitation
                    if str(type(confObj[firstLevelKey])) == "<class 'dict'>":
                        for secondLevelKey in confObj[firstLevelKey]:
                            value = confObj[firstLevelKey][secondLevelKey]
                            m = re.search(r'^.*\$([A-Z\_]+)[^A-Z\_].*$',value)
                            if m:
                                varFound = m.group(1)
                                result = re.sub(r'\$' + varFound,os.environ[varFound],value)

                                # Replacing vars on config value to your respective values
                                confObj[firstLevelKey][secondLevelKey] = result
        except Exception as e:
            print("Fail loading config file '{}' - {}".format(configFile,str(e)))
            raise

        return confObj


if __name__ == '__main__':
    pass
