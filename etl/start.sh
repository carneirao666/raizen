#!/bin/bash

################################################################################

## Nome        : start.sh
## Description : Initialize the nasa-log pySpark script

################################################################################

###  ..:: Workflow ::..

## 1 - Deletes the Python cache files

## 2 - Checks if script is already in execution

## 3 - Configures and runs 'spark-submit' command



################################################################################

### ..::Environment::..###

## Job
JOB_NAME="etl"

## Spark
SPARK_MASTER='local[*]'
SPARK_MODE='client'

## etl
export HDFS_BASE_DIR=/user/$USER/etl
LOCK_FILE="$JOB_NAME.lock"
PYSPARK_FILE_MAIN="$JOB_NAME.py"


################################################################################

### ..:: Funcoes ::..


function write_log() {
    echo "$1 [$(date '+%Y-%m-%d - %H:%M:%S')] - $2" 
}



function start_pyspark() {
  # write_log "[INFO]" " - Executando spark submit para o job ${JOB_NAME}."
  spark-submit \
      --name ${JOB_NAME^^} \
      --master ${SPARK_MASTER} \
      --driver-memory 1g \
      --conf spark.driver.cores=1 \
      --conf spark.executor.cores=10 \
      --py-files ${PYSPARK_FILE_MAIN} "${PYSPARK_FILE_MAIN}"
       # --deploy-mode ${SPARK_MODE} \
       # --driver-memory 1G \
       # --executor-memory 500M \
       # --conf spark.executor.cores=8 \
       # --conf spark.driver.cores=2 \
       # --conf spark.driver.memoryOverhead=4096 \
       # --conf spark.executor.memoryOverhead=4096 \
       # --conf spark.yarn.maxAppAttemps=4 \
       # --conf spark.task.maxFailures=4 \
       # --conf spark.speculation=true \
       # --conf spark.dynamicAllocation.enabled=true \
       # --conf spark.dynamicAllocation.initialExecutors=2 \
       # --conf spark.dynamicAllocation.maxExecutors=8 \
       # --conf spark.dynamicAllocation.minExecutors=2 \
       # --conf spark.shuffle.service.enabled=true \
       # --conf spark.shuffle.service.port=7337 \
       # --jars ./jars/mysql-connector-java-8.0.14.jar \

  if [ $? == 0 ]
  then
    write_log "[INFO]" "Análise de logs finalizada com sucesso!"
  else
    write_log "[ERROR]" "Falha na análise de log"
    delete_running
    exit 1
  fi

}



function isrunning() {

    FILE_PATH="${HDFS_BASE_DIR}/${LOCK_FILE}"

    hdfs dfs -test -f $FILE_PATH

    if [ $? == 0 ]
    then
            write_log "[WARN]" "Processo ${JOB_NAME} em andamento."
            exit 1
    else
            hdfs dfs -touchz $FILE_PATH
            write_log "[INFO]" "Arquivo ($LOCK_FILE) criado."
            write_log "[INFO]" "Processo de carga ${JOB_NAME} iniciado."
    fi

}





function delete_running() {

    FILE_PATH="${HDFS_BASE_DIR}/${LOCK_FILE}"

    hdfs dfs -test -f $FILE_PATH

    if [ $? == 0 ]
    then
            hdfs dfs -rm -f -r -skipTrash $FILE_PATH
            write_log "[INFO]" "Arquivo ($LOCK_FILE) deletado."

    else
            write_log "[WARN]" "Arquivo ($LOCK_FILE) nao encontrado."
            exit 1
    fi

}




function main() {
  write_log "[INFO]" "Inicio do script ${JOB_NAME}.sh."

  write_log "[INFO]" "Apagando os arquivos de cache do Python"
  find .  -name "__pycache__" -exec rm -rf {} \;
  find . -type f -name "*.pyc" -exec rm -f {} \;

  #delete_running
  isrunning

  write_log "[INFO]" "INICIANDO O SCRIPT PYSPARK ${JOB_NAME}.py"
  start_pyspark

  if [ $? == 0 ]
  then
    write_log "[INFO]" "SCRIPT ${JOB_NAME}.py FINALIZADO COM SUCESSO!"

  else
    write_log "[ERROR]" "PROBLEMAS NA EXECUÇÃO DO SCRIPT ${JOB_NAME}.py"
    delete_running
  fi

  delete_running
}


main

exit 0

