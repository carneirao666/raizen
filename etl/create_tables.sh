#! /bin/bash

hive -f ddl/create_raw_table.hql  
hive -f ddl/create_sales_diesel_by_uf_type.hql  
hive -f ddl/create_sales_odf_uf_product.hql  
hive -f ddl/create_stage_table.hql
