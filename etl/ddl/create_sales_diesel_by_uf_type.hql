set hive.exec.dynamic.partition.mode=nonstrict;
DROP TABLE refined_sales_diesel_by_uf_type;
CREATE TABLE refined_sales_diesel_by_uf_type(
	year_month  date,
	product     string,
	ano         string,
	uf          string,
	volume      double,
	created_at timestamp
)PARTITIONED BY(year_month_str string)
STORED AS ORC
TBLPROPERTIES('ORC.COMPRESS'='SNAPPY');
