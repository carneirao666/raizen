set hive.exec.dynamic.partition.mode=nonstrict;
DROP TABLE raw_sales;
CREATE TABLE raw_sales( 
  COMBUSTIVEL string, 
  REGIAO string, 
  ESTADO string,
  Jan string, 
  Fev string,
  Mar string,
  Abr string,
  Mai string,
  Jun string,
  Jul string,
  Ago string,
  `Set` string,
  `Out` string,
  Nov string,
  Dez string,
  TOTAL string,
	created_at timestamp 
)PARTITIONED BY(ANO string) 
STORED AS ORC 
TBLPROPERTIES('ORC.COMPRESS'='ZLIB');
