set hive.exec.dynamic.partition.mode=nonstrict;
DROP TABLE refined_sales_odf_per_uf_product;
CREATE TABLE refined_sales_odf_per_uf_product(
	year_month  date,
	product     string,
	unit        string,
	uf          string,
	volume      double,
	created_at timestamp
)PARTITIONED BY(year_month_str string)
STORED AS ORC
TBLPROPERTIES('ORC.COMPRESS'='SNAPPY');
