set hive.exec.dynamic.partition.mode=nonstrict;
DROP TABLE stage_sales;
CREATE TABLE stage_sales(
  product     string,
  ano         string,
  uf          string,
  volume      double,
  created_at timestamp
)PARTITIONED BY(year_month string)
STORED AS ORC
TBLPROPERTIES('ORC.COMPRESS'='SNAPPY');
