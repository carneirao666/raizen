import pyspark
import pyspark.sql.functions as F
import re
import sys
import pandas as pd
import warnings


from utils.base import *
from utils.file_tools import *
from pyspark.sql.functions import *
from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql import Row

# Globals
# This came from base module
logger = getLogger('raizen-etl')



def checkRejected(conf,dataSetPaths):
    """Check if some path passed on dataSetPaths parameter is on rejected dir. If it is, returns True.
    Otherwise, False.

        Parameters
        ----------
        
        conf: dict, required
        Dictionary object with JSON config data.

        dataSetPaths: list(strings), required
        Possible paths that can be included on rejected dir

        Returns
        -------

        True if some file is in rejected dir. Otherwise, False

    """
    for dataset in dataSetPaths:
        PathParts = re.split(r'\/',dataset)
        fileName = PathParts[-1]
        fileName = re.sub(r'\.gz','',fileName)

        logger.info("Verificando arquivo: {}".format(fileName))
        rejectedDir = conf['hdfsDirs']['rejected']
        msg = []
        try:
            msg = runDFSCmd('-ls {}'.format('/'.join([rejectedDir, fileName])))
        except Exception as e:
            pass

        if len(msg) > 0:
            logger.error("O arquivo '{}' está na área 'rejected'({}). Verifique antes de continuar!".format(fileName, rejectedDir))
            exit(1)

    return True



def startPreProcessing(conf):
    """Loads config from config file, create HDFS dirs, make downloads and put the download files
        on 'processing' directory in HDFS.

        Parameters
        ----------

        conf: dict, required
        Dictionary object with JSON config data.

        Returns
        -------

        Nothing

    """

    #logger.info("Criando diretórios no HDFS, se necessário")
    mkHDFSDirs(conf)

    # Download
    dataSetPaths = conf['dataSetPaths']
    downloadPath = conf['localDirs']['download']

    logger.info("Verificando se os arquivos para download estão na área 'rejected'")
    checkRejected(conf,dataSetPaths)

    logger.info("Fazendo download dos arquivos e salvando localmente em '{}'".format(downloadPath))
    #downloadDataSets(dataSetUrls,downloadPath)
    # Mocking "download"
    import os
    for ds in dataSetPaths:
        os.system(f"cp {ds} {downloadPath}")





def startProcessing(spark,conf):
    """Starts processing excel files


        Parameters
        ----------

        spark SparkSession object
        SparkSession instance

        conf dict
        A dictionary with configuration defined on conf/default.json 

        Return
        ------

        Nothing
    """
    
    downloadPath = conf['localDirs']['download']
    processingDir = conf['hdfsDirs']['processing']
    logger.info("Copiando os arquivos de downloads para /tmp")
    os.system(f"cp {downloadPath}/*.xlsx /tmp")
    logger.info("Movendo os datasets localizados em '{}' para o HDFS no diretório '{}'".format(downloadPath,processingDir))
    moveToHDFS(downloadPath,processingDir)
    logger.info(f"Copiando os datasets localizados em '{downloadPath}' para '/tmp'")


    # Reading files on processing dir
    results = runDFSCmd("-ls {}".format(processingDir))
    if len(results) > 0:
      results.pop(0)  
    Paths = []
    df = None
    for rawFile in results:
        if len(rawFile) < 2: # This will break 
            continue
        else:
            (permissions,replicas,owner,group,size,date,hour,path) = re.split(r'\s+',rawFile)
            if re.search(r'^-.*$',permissions) and len(path) > 0:
                Paths.append(path)

    if len(Paths) == 0:
        logger.info("Sem dados para processar!")
        exit(0)
    else:
        for path in Paths:
            logger.info("Processando o arquivo {}".format(path))

            ########

            # Parseando o xlsx
            logger.info(f"parseando o arquivo xlx {path}")
            xl = None

            filename = path.split('/')[-1]
            tmp_path = '/' + '/'.join(['tmp',filename])
            xl = pd.ExcelFile(tmp_path)

            # All data
            logger.info("Recuperando os 'sheets' do arquivo xlsx")
            sheet_names = xl.sheet_names
            # Data that matters
            sheet_names = sheet_names[1:4]
            logger.info(f"Nomes dos sheets a serem processados: {sheet_names}")

            # Concatenanting dataframes
            adf = []
            for sheet in sheet_names[0:3]:
                adf.append(xl.parse(sheet))
            pd_df = pd.concat(adf)
            adf = None
            raw_df_length = len(pd_df)
            logger.info(f"Tamanho do dataframe inicial: {raw_df_length}")

            # Processando dados na camada 'raw'
            logger.info("Processando dados na camada 'raw'")
            raw_df = start_raw_processing(spark, pd_df)
            raw_df.cache()

            # Processando dados na camada 'stage'
            logger.info("Processando dados na camada 'stage'")
            stage_df = start_stage_processing(spark, pd_df)
            stage_df.cache()

            # Camada 'refined' - Respondendo as perguntas
            # Pergunta 1. Sales of oil derivative fuels by UF and product
            logger.info("Gerando dados da questão 1")
            refined1_df = anwser_question_1(stage_df)
            refined1_df.show()

            logger.info("Gerando dados da questão 2")
            refined2_df = answer_question_2(stage_df)
            refined2_df.show()


def answer_question_2(stage_df):
    """Gets values asked in question 2 of doc 'docs/razien_Readme.md', try to write on Hive
    and return a new dataframe with answer data in a form of DataFrame object

        Parameters
        ----------

        stage_df Spark DataFrame object

        the stage data for answer data extraction

        Returns
        -------

        A Spark Dataframe

    """
    # List of 'types' of products
    types = [
      "'ÓLEO DIESEL (OUTROS ) (m3)'",
      "'ÓLEO DIESEL S-10 (m3)'",
      "'ÓLEO DIESEL S-1800 (m3)'",
      "'ÓLEO DIESEL MARÍTIMO (m3)'",
      "'ÓLEO DIESEL S-500 (m3)'"
    ]
    in_str = ','.join(types)

    # Answer the firs request
    df_refined2 = stage_df.where(f" product IN ({in_str}) ") \
                .groupBy('ano','year_month','uf','product','unit').sum() \
                .withColumn('year_month_str', F.regexp_replace(F.col('year_month').cast('string'),r'-','')) \
                .withColumn('created_at',F.current_timestamp()) \
                .select(F.from_unixtime(F.unix_timestamp('year_month', 'yyyy-mm')).cast(DateType()).alias('year_month'),
                    'product','unit','uf',
                    F.col('sum(volume)').alias('volume'),
                    F.col('created_at'),
                    'year_month_str'
                )

    # df_refined2.cache() 
    
    # Spark is too slow to write on Hive. So, I'm creating slices by column year_month
    year_month = stage_df.select('year_month').distinct().rdd.flatMap(lambda x: x).collect()
    for ym in year_month:
        df_refined2.where(F.col('year_month_str') == F.lit(ym)) \
            .write \
            .format('orc') \
            .insertInto('refined_sales_odf_per_uf_product',overwrite=True)

    return df_refined2

def anwser_question_1(stage_df):
    """Gets values asked in question 1 of doc 'docs/razien_Readme.md', try to write on Hive

    and return a new dataframe with answer data in a form of DataFrame object

        Parameters
        ----------

        stage_df Spark DataFrame object

        the stage data for answer data extraction

        Returns
        -------
    
        A Spark Dataframe
    
    """
    # Lista de produtos da tabela 'Vendas, pelas distribuidoras¹, dos derivados combustíveis de petróleo por Unidade da Federação e produto - 2000-2020 (m3)'
    oil_derivate_fuel = [
        "'ETANOL HIDRATADO (m3)'",
        "'GASOLINA C (m3)'",
        "'GASOLINA DE AVIAÇÃO (m3)'",
        "'GLP (m3)'",
        "'ÓLEO DIESEL (m3)'",
        "'QUEROSENE DE AVIAÇÃO (m3)'",
        "'QUEROSENE ILUMINANTE (m3)'"
    ]
    # Building IN columns
    in_str = ','.join(oil_derivate_fuel)

    df_year = stage_df.select('year_month').distinct()

    # Answer the firs request
    df_refined1 = stage_df.where(f"product IN ({in_str})") \
                    .groupBy('year_month','uf','product','unit').sum() \
                    .withColumn('year_month_str', F.regexp_replace(F.col('year_month').cast('string'),r'-','')) \
                    .withColumn('created_at',F.current_timestamp()) \
                    .select(F.from_unixtime(F.unix_timestamp('year_month', 'yyyy-mm')).cast(DateType()).alias('year_month'),
                        'product','unit','uf',
                        F.col('sum(volume)').alias('volume'),
                        F.col('created_at'),'year_month_str'
                        
                        
                    )
    # Spark is too slow to write on Hive. So, I'm creating slices by column year_month
    year_month = stage_df.select('year_month').distinct().rdd.flatMap(lambda x: x).collect()
    for ym in year_month:
        df_refined1.where(F.col('year_month') == F.lit(ym)) \
            .write \
            .format('orc') \
            .insertInto('refined_sales_odf_per_uf_product',overwrite=True)
    
    return df_refined1





def start_stage_processing(spark, pd_df):
    """Gets raw data pre-processed by pandas, make transformations, converts to Spark DataFrame,
        writes on Hive return this dataframe
    """
    # Replacing NaN to 0.0
    clist = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez', 'TOTAL']
    for c in clist:
        pd_df[c].fillna(0.0, inplace=True)

    ## Getting one df per month
    columns = list(pd_df.columns)
    base_columns = columns[0:4]
    months = clist[0:-1]
    per_month = dict()

    # Brazillian portuguese month abbreviature to number
    month_to_number = {
        'Jan': '01',
        'Fev': '02',
        'Mar': '03',
        'Abr': '04',
        'Mai': '05',
        'Jun': '06',
        'Jul': '07',
        'Ago': '08',
        'Set': '09',
        'Out': '10',
        'Nov': '11',
        'Dez': '12'
    }

    # Construindo uma ista de DataFrames
    dfs = []
    for m in months:
        ucolumns = base_columns
        ucolumns.append(m)

        # Selecionando apenas colunas a serem processadas
        per_month[m] = pd_df[ucolumns]
        # Renaming columns to client standards defined on documentation
        per_month[m].rename({m: 'volume'}, axis='columns', inplace=True)
        per_month[m].rename({'COMBUSTÍVEL': 'product'}, axis='columns', inplace=True)
        per_month[m].rename({'ESTADO': 'uf'}, axis='columns', inplace=True)

        # Concatenando year_month com ANO
        per_month[m].loc[:, 'year_month'] = per_month[m].loc[:, 'ANO'].apply(
            lambda x: str(x) + '-' + month_to_number[m])

        # Dropando a coluna REGIÃO
        per_month[m] = per_month[m].drop('REGIÃO', 1)

        # Atualizando o array de dataframes
        dfs.append(per_month[m])

        # Evitando acumular as colunas de mês no Dataframe corrente
        ucolumns.pop()

    # Concatenando todos os dataframes da lista
    wdf = pd.concat(dfs)
    dfs = None

    # Convertendo os dataframes concatenados para spark
    stage_df = spark.createDataFrame(wdf)

    # Lista de colunas ordenadas
    columns_order = ['year_month', 'ANO', 'uf', 'product', 'unit', 'volume', 'created_at']

    # Adding created_at column
    stage_df = stage_df.withColumn('created_at', F.current_timestamp())

    # Extract unit info
    stage_df = stage_df.withColumn('unit', F.regexp_extract(F.col('product'), r'^.*?\ \(([0-9A-Za-z]+)\)$', 1)) \
        .select(columns_order)

    # Saving some memory
    wdf = None

    # Salvando os dados na tabel stage_sales
    created_columns_order = ['product', 'ANO', 'uf', 'volume', 'created_at', 'year_month']
    stage_df.select(created_columns_order) \
        .write \
        .format('orc') \
        .insertInto('stage_sales',overwrite=True)

    return stage_df


def start_raw_processing(spark, pd_df):
    """Parses a 'xlsx' file, extract data from sheets, transform this data in a Pandas Dataframe object,
    writes on Hive and return this raw dataframe

    """
    ## Converter para spark dataframe é mais conveniente para conversão e dados e também para salvar dados no hive
    # usando formato ORC

    # Renomeando campos com nomes incompatíveis com o Hive
    raw_df = spark.createDataFrame(pd_df) \
        .withColumnRenamed('COMBUSTÍVEL','combustivel') \
        .withColumnRenamed('REGIÃO','regiao')

    # Adicionando 'created_at' p
    raw_df = raw_df.withColumn('created_at',F.current_timestamp())

    # Salvando a tabela ORC
    created_column_order = ['combustivel','regiao','estado','Jan','Fev','Mar','Abr','Mar','Mai','Jun','Ago','Set','Out','Nov','Dez','TOTAL','created_at','ANO']
    logger.info("Salvando dados do dataframe 'raw' na tabela 'raw_sales', no Hive")
    raw_df.select(created_column_order) \
        .write \
        .format('orc') \
        .insertInto('raw_sales',overwrite=True)

    return raw_df


def main():
    """Creates the "infrastructure" to start the ETL process and starts the prcoess

    """

    # Getting config
    logger.info("Carregando a configuração em 'conf/default.json'") # TODO colocar o path de conf em uma var de amb.
    conf = getConfig()

    # HDFS data dirs
    processingDir = conf['hdfsDirs']['processing']
    processedDir = conf['hdfsDirs']['processed']
    rejectedDir = conf['hdfsDirs']['rejected']

    ## Pre-processamento
    startPreProcessing(conf)

    # Iniciando o spark e o processamento dos arquivos.
    try:
        logger.info("Iniciando a sessão do Spark")
        spark = getSpark()
        logger.info("Iniciando a análise dos logs")
        startProcessing(spark,conf)    
    except Exception as e:
        logger.error("Falha no processamento! {}".format(str(e)))
        logger.info( "Movendo arquivos de '{}' para '{}' ".format('/'.join([processingDir, '*']), rejectedDir) )
        runDFSCmd("-mv {} {}".format('/'.join([processingDir, '*']), rejectedDir) )
        # TODO check
        sys.exit(1)

    # Se tudo der certo, move os arquivos para processed
    logger.info( "Movendo arquivos de '{}' para '{}' ".format('/'.join([processingDir,'*']), processedDir) )
    try:
        runDFSCmd("-mv {} {}".format('/'.join([processingDir, '*']), processedDir) )
    except Exception as e:
        error = str(e)
        if re.search(r'file exists',error,re.IGNORECASE):
            runDFSCmd("-cp -f {} {}".format('/'.join([processingDir, '*']), processedDir) )
            runDFSCmd("-rm {}".format('/'.join([processingDir, '*'])) )
            pass
        else:
            logger.error("Problemas para mover os arquivos de '{}' para '{}' - {}".format(processingDir, processedDir, str(e)))
            sys.exit(1)
    
    # TODO check

if __name__ == '__main__':
    main()
    sys.exit(0)


